package week5.day1;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class LearnProperty {
	
	@Test
	public void Property() throws FileNotFoundException, IOException {
		Properties prop=new Properties();
		
		prop.load(new FileInputStream("English.properties"));
		
		/*String property = prop.getProperty("LoginPage.username.id");
		System.out.println(property);*/
		
		
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("http://leaftaps.com/opentaps");
		driver.manage().window().maximize();
		
		driver.findElementById(prop.getProperty("LoginPage.username.id")).sendKeys("DemoSalesManager");
		driver.findElementById(prop.getProperty("LoginPage.password.id")).sendKeys("crmsfa");
		
		driver.findElementByClassName(prop.getProperty("LoginPage.loginButton.className")).click();
	}

}
