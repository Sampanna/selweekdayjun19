package com.testleaf.leaftaps.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.autoBot.testng.api.base.Annotations;
import com.testleaf.leaftaps.pages.LoginPage;

public class Tc001_LogintoLeaftaps extends Annotations {
	@BeforeTest
	public void setData() {
		excelFileName="TC001";
		testcaseName="Login and Create Lead";
		testcaseDec="Create lead positive flow";
		author="Sampanna";
		category= "Functional";
		
	}
	
	@Test (dataProvider="fetchData")
	public void Login (String data[]) {
		new LoginPage()
		.enterUserName(data[0])
		.enterPassword(data[1])
		.clickLogin()
		.clickCRMSFA()
		.clickLeads()
		.clickCreateLead()
		.enterCompanyName(data[2])
		.enterFirstName(data[3])
		.enterLastName(data[4])
		.clickCreateLead()
		.getPageTitle();
	}
	

}
