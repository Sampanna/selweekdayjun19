package com.testleaf.leaftaps.pages;

import com.autoBot.testng.api.base.Annotations;

public class MyHomePage extends Annotations{
	
	public MyHomePage() {}
	
	public MyLeadsPage clickLeads() {
		click(locateElement("link", "Leads"));
		return new MyLeadsPage();
	}

}
