package com.testleaf.leaftaps.pages;

import com.autoBot.testng.api.base.Annotations;

public class CreateLeadPage extends Annotations{
	
	public CreateLeadPage() {}
	
	
	
	public CreateLeadPage enterCompanyName(String cname) {
		clearAndType(locateElement("id", "createLeadForm_companyName"),cname);
		return this;
	}
	
	public CreateLeadPage enterFirstName(String fname) {
		clearAndType(locateElement("id", "createLeadForm_firstName"),fname);
		return this;
	}

	public CreateLeadPage enterLastName(String lname) {
		clearAndType(locateElement("id", "createLeadForm_lastName"),lname);
		return this;
	}
	
	public ViewLeadPage clickCreateLead() {
		click(locateElement("class", "smallSubmit"));
		return new ViewLeadPage();
	}
}
