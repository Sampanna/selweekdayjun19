package com.testleaf.leaftaps.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.autoBot.testng.api.base.Annotations;

public class HomePage extends Annotations{
	
	public HomePage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy (how=How.LINK_TEXT, using="CRM/SFA")
	private WebElement eleCRMSFA;
	
	public MyHomePage clickCRMSFA() {
		//click(locateElement("link", "CRM/SFA"));
		click(eleCRMSFA);
		return new MyHomePage();
	}

}
